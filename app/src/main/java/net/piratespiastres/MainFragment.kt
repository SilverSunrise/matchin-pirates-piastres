package net.piratespiastres

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment() {

    companion object {
        private var numberOfClick: Int = 0

        private lateinit var pirateCard: List<PirateCard>

        private var remainsPair: Int = 8

        private lateinit var btnPirateCard: List<ImageButton>

        private var firstIndex: Int? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        btnPirateCard = listOf(
            view.findViewById(R.id.pirateCard1),
            view.findViewById(R.id.pirateCard2),
            view.findViewById(R.id.pirateCard3),
            view.findViewById(R.id.pirateCard4),
            view.findViewById(R.id.pirateCard5),
            view.findViewById(R.id.pirateCard6),
            view.findViewById(R.id.pirateCard7),
            view.findViewById(R.id.pirateCard8),
            view.findViewById(R.id.pirateCard9),
            view.findViewById(R.id.pirateCard10),
            view.findViewById(R.id.pirateCard11),
            view.findViewById(R.id.pirateCard12),
            view.findViewById(R.id.pirateCard13),
            view.findViewById(R.id.pirateCard14),
            view.findViewById(R.id.pirateCard15),
            view.findViewById(R.id.pirateCard16)
        )

        val btnPirateCardImages: MutableList<Int> = mutableListOf(
            R.drawable.pirates_card_1,
            R.drawable.pirates_card_2,
            R.drawable.pirates_card_3,
            R.drawable.pirates_card_4,
            R.drawable.pirates_card_5,
            R.drawable.pirates_card_6,
            R.drawable.pirates_card_7,
            R.drawable.pirates_card_8
        )

        btnPirateCardImages.addAll(btnPirateCardImages)
        btnPirateCardImages.shuffle()

        restartGame(view)

        pirateCard = btnPirateCard.indices.map { number ->
            PirateCard(btnPirateCardImages[number])
        }

        btnPirateCard.forEachIndexed { indexCard, pirateCard ->
            pirateCard.setOnClickListener {
                updateDataPirateCard(indexCard)
                updateViewPirateCard()
                numberOfClick += 1
                tvClickScore.text = "Clicks: $numberOfClick"
            }
        }

        updateViewPirateCard()

        return view
    }

    private fun updateDataPirateCard(indexCard: Int) {
        val pirateItem = pirateCard[indexCard]
        if (pirateItem.isFlipped) {
            numberOfClick -= 1
            return
        }
        firstIndex = if (firstIndex == null) {
            flipChanges()
            indexCard
        } else {
            isPiratePairConfirmed(firstIndex!!, indexCard)
            null
        }
        pirateItem.isFlipped = !pirateItem.isFlipped
    }

    private fun flipChanges() {
        for (symbol in pirateCard) {
            if (!symbol.isPair) {
                symbol.isFlipped = false
            }
        }
    }

    private fun restartGame(view: View) {
        val btnRestart = view.findViewById(R.id.btnRestart) as Button
        btnRestart.setOnClickListener {
            val mainFragment = MainFragment()
            fragmentManager?.beginTransaction()?.replace(R.id.fragment_container, mainFragment)
                ?.commit()
            remainsPair = 0
            numberOfClick = 0
        }
    }

    private fun updateViewPirateCard() {
        pirateCard.forEachIndexed { date, item ->
            val button = btnPirateCard[date]
            if (item.isPair) {
                button.alpha = 0.58f
            }
            button.setImageResource(if (item.isFlipped) item.look else R.drawable.card)
        }
    }

    private fun isPiratePairConfirmed(firstCard: Int, secondCard: Int) {
        if (pirateCard[firstCard].look == pirateCard[secondCard].look) {
            pirateCard[firstCard].isPair = true
            pirateCard[secondCard].isPair = true
            remainsPair -= 1
            if (remainsPair == 0) {
                iv_epicWin.animate().apply {
                    iv_epicWin.visibility = View.VISIBLE
                    duration = 500
                    alpha(0.8f)
                }.withEndAction {
                    iv_epicWin.animate().apply {
                        duration = 500
                        alpha(1.0f)
                    }
                }.start()
                btnRestart.visibility = View.VISIBLE
            }
            tvScore.text = "Remains pairs: $remainsPair"
        }
    }

}