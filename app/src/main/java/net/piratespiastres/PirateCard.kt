package net.piratespiastres

data class PirateCard(var look: Int, var isFlipped: Boolean = false, var isPair: Boolean = false)